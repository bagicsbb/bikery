//
//  CitiesData.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 30..
//

import Foundation

// MARK: - CitiesData
struct DataModel: Codable {
    let data: [PlaceData]
}

// MARK: - Datum
struct PlaceData: Codable, Identifiable {
    let name, summary: String
    let type: TypeEnum
    let lat, lon: Double
    let id: String
    let iconURL: String

    enum CodingKeys: String, CodingKey {
        case name, summary, type, lat, lon, id
        case iconURL = "icon_url"
    }
}

enum TypeEnum: String, Codable {
    case address = "address"
}
