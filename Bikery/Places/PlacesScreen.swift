//
//  ContentView.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 27..
//

import SwiftUI

struct PlacesScreen: View {
    
    @ObservedObject var viewModel: PlacesScreenViewModel
    
    var body: some View {
        NavigationView {
            ZStack {
                content
                loadingView
            }
            .onDisappear(perform: { viewModel.address = "" })
            .navigationBarTitleDisplayMode(.inline)
            .searchable(text: $viewModel.address, placement: .navigationBarDrawer(displayMode: .always))
        }
    }
    
    // MARK: - Loading

    @ViewBuilder
    var loadingView: some View {
        if viewModel.isLoading {
            ZStack {
                Color.black
                    .opacity(0.4)
                    .edgesIgnoringSafeArea(.all)
                loadingIndicator
            }
        }
    }

    var loadingIndicator: some View {
        ProgressView()
            .progressViewStyle(
                CircularProgressViewStyle(tint: .white)
            )
            .scaleEffect(1.7, anchor: .center)
    }
    
    var list: some View {
        List {
            ForEach(viewModel.data, id: \.id) { data in
                PlaceCell(viewModel: PlaceCellViewModel(data: data, updateData: viewModel.updateData, isFavoriteItem: viewModel.isFavorite(data: data)))
            }
        }
        .listStyle(.plain)
    }
    
    var emptyScreen: some View {
        VStack(spacing: 30) {
            Image(systemName: "arrow.up")
                .resizable()
                .frame(width: 40, height: 80, alignment: .center)
                .padding(.top, 40)
                .animation(.linear(duration: 2).repeatForever())
                
            Text("Start typing the name of a city.")
                .font(.system(size: 40, weight: .medium, design: .monospaced))
                .foregroundColor(.gray)
                .padding(.horizontal, 40)
                .multilineTextAlignment(.center)
            Spacer()
        }
    }
    
    @ViewBuilder
    var content: some View {
        if viewModel.data.isEmpty {
            emptyScreen
        } else {
            list
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        PlacesScreen(viewModel: PlacesScreenViewModel())
    }
}
