//
//  CitiesScreenViewModel.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 30..
//

import Foundation
import Combine

class PlacesScreenViewModel: ObservableObject {
    
    @Published var data: [PlaceData] = []
    @Published var address = ""
    @Published var isLoading = false
    
    private var cancellables = Set<AnyCancellable>()
    let repository = DataRepository()

    init() {
        addSubscriber()
    }
    
    func addSubscriber() {
        $address
            .debounce(for: 1, scheduler: DispatchQueue.main)
            .sink(receiveValue: { [weak self] (text) in
                if !text.isEmpty && text.validCharacters() == self?.address.count {
                    self?.getData(for: text.replacingOccurrences(of: " ", with: "%20"))
                } else {
                    self?.data = []
                }
            })
            .store(in: &cancellables)
    }
    
    private func getData(for search: String) {
        isLoading = true
        repository.getDataForAddress(address: search)
            .sink(receiveCompletion: handleGetData, receiveValue: value)
            .store(in: &cancellables)
    }
    
    private func handleGetData(_ completion: Subscribers.Completion<DataRepositoryError>) {
        isLoading = false
    }
    
    private func value(_ value: DataModel) {
        data = value.data
        isLoading = false
    }
}

extension PlacesScreenViewModel {
    //MARK: - DataService Functions
    
    func updateData(data: PlaceData) {
        DataService.shared.updateData(placeData: data)
    }
    
    func isFavorite(data: PlaceData) -> Bool {
        DataService.shared.isFavoriteItem(placeData: data)
    }
}
