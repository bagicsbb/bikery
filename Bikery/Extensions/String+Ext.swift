//
//  String+Ext.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 31..
//

import Foundation

extension String {
    func validCharacters() -> Int {
        let range = NSRange(location: 0, length: self.utf16.count)
        let regexPattern = try! NSRegularExpression(pattern: Constant.pattern)
        return regexPattern.numberOfMatches(in: self, options: [], range: range)
    }
}
