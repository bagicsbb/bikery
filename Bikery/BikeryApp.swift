//
//  BikeryApp.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 27..
//

import SwiftUI

@main
struct BikeryApp: App {
    var body: some Scene {
        WindowGroup {
            TabView {
                PlacesScreen(viewModel: PlacesScreenViewModel())
                    .tabItem {
                        VStack {
                            Image(systemName: "magnifyingglass")
                            Text("Search")
                        }
                    }
                    
                FavoritesScreen(viewModel: FavoritesScreenViewModel())
                    .tabItem {
                        VStack {
                            Image(systemName: "heart.fill")
                            Text("Favorites")
                        }
                    }
            }
            .preferredColorScheme(.light)
        }
    }
}
