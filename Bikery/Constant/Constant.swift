//
//  Constant.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 31..
//

import Foundation

struct Constant {
    
    static let pattern = "[a-zA-Z0-9\\s]"
    static let favoritesScreenTitle = "Favorites"
    static let dataModelName = "DataModel"
    static let entityName = "DataEntity"
    static let heartFill = "heart.fill"
    static let heart = "heart"
    static let baseURL = "https://api.bikecitizens.net/api/v2/search?q="
    
}
