//
//  MapView.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 31..
//

import SwiftUI
import MapKit

struct MapView: View {
    
    @ObservedObject var viewModel: MapViewModel
    var body: some View {
        Map(coordinateRegion: $viewModel.region, annotationItems: viewModel.annotation) { annotation in
            MapMarker(coordinate: CLLocationCoordinate2D(latitude: annotation.lat, longitude: annotation.lon), tint: .red)
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(viewModel: MapViewModel(data: PlaceData(name: "", summary: "", type: .address, lat: 47.0708678, lon: 15.4382786, id: "", iconURL: "")))
    }
}
