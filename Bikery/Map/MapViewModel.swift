//
//  MapViewModel.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 31..
//

import Foundation
import MapKit

class MapViewModel: ObservableObject {
    @Published var region: MKCoordinateRegion
    var annotation: [PlaceData] = []
    
    init(data: PlaceData) {
        self.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: data.lat, longitude: data.lon), span: MKCoordinateSpan(latitudeDelta: 0.006, longitudeDelta: 0.006))
        annotation.append(data)
    }
}
