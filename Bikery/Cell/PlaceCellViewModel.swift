//
//  PlaceCellViewModel.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 30..
//

import Foundation
import Combine

class PlaceCellViewModel: ObservableObject {
    
    @Published var data: PlaceData
    @Published var isFavoriteItem: Bool
    
    let updateData: (PlaceData) -> ()

    init(data: PlaceData, updateData: @escaping (PlaceData) -> (), isFavoriteItem: Bool) {
        self.data = data
        self.updateData = updateData
        self.isFavoriteItem = isFavoriteItem
    }
}
