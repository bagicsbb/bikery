//
//  PlaceCell.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 30..
//

import SwiftUI
import SDWebImageSwiftUI

struct PlaceCell: View {
    
    @ObservedObject var viewModel: PlaceCellViewModel
    @State var openSheet: Bool = false

    var body: some View {
        HStack {
            placeImage
            placeDetails
            Spacer()
            favoriteIcon
        }
        .contentShape(Rectangle())
        .onTapGesture {
            openSheet = true
        }
        .halfsheet(showSheet: $openSheet) {
            sheetContent
        } onEnd: {
            openSheet = false
        }
    }
    
    var sheetContent: some View {
        ZStack {
            Color.white
            VStack(spacing: 0) {
                placeImageBig
                placeDetailsTitle
                placeDetailsSummary
                mapView
            }
            favoriteImageBig
        }
        .foregroundColor(.black)
        .edgesIgnoringSafeArea(.all)
    }
    
    var placeDetails: some View {
        VStack(alignment: .leading) {
            Text(viewModel.data.name)
            Text(viewModel.data.summary)
                .foregroundColor(Color(UIColor.lightGray))
                .lineLimit(1)
        }
    }
    
    var placeImage: some View {
        WebImage(url: URL(string: viewModel.data.iconURL))
            .resizable()
            .frame(width: 50, height: 50)
            .padding(.leading, 5)
            .padding(.trailing, 10)
    }
    
    var favoriteIcon: some View {
        Image(systemName: viewModel.isFavoriteItem ? Constant.heartFill : Constant.heart)
            .resizable()
            .frame(width: 22, height: 20)
            .padding(.horizontal, 10)
            .onTapGesture {
                viewModel.updateData(viewModel.data)
                viewModel.isFavoriteItem.toggle()
            }
    }
    
    var mapView: some View {
        MapView(viewModel: MapViewModel(data: viewModel.data))
            .frame(width: 303, height: 163)
            .padding(.horizontal, 44)
            .padding(.bottom, 30)
    }
    
    var placeImageBig: some View {
        WebImage(url: URL(string: viewModel.data.iconURL))
            .resizable()
            .frame(width: 120, height: 120)
            .padding(.top, 20)
            .padding(.bottom, 5)
    }
    
    var placeDetailsTitle: some View {
        Text(viewModel.data.name)
            .font(Font.largeTitle)
            .fontWeight(.bold)
            .padding(.bottom, 10)
            .padding(.horizontal, 20)
            .minimumScaleFactor(0.6)
    }
    
    var placeDetailsSummary: some View {
        Text(viewModel.data.summary)
            .foregroundColor(Color(UIColor.lightGray))
            .padding(.bottom, 20)
            .padding(.horizontal, 20)
            .lineLimit(1)
    }
    
    var favoriteImageBig: some View {
        ZStack(alignment: .topTrailing) {
            VStack {
                HStack {
                    Spacer()
                    Image(systemName: viewModel.isFavoriteItem ? Constant.heartFill : Constant.heart)
                        .resizable()
                        .frame(width: 28, height: 26)
                        .padding(.vertical, 30)
                        .padding(.trailing, 40)
                }
                .onTapGesture {
                    openSheet = false
                    viewModel.updateData(viewModel.data)
                    viewModel.isFavoriteItem.toggle()
                }
                Spacer()
            }
        }
    }
}
