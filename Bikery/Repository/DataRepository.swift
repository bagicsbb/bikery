//
//  DataRepository.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 30..
//

import Foundation
import Combine

enum DataRepositoryError: Error {
    case customError
}

class DataRepository {
    
    private var cancellables = Set<AnyCancellable>()
    let networkService = NetworkService()
    
    public func getDataForAddress(address: String) -> AnyPublisher<DataModel, DataRepositoryError> {
        return Future<DataModel, DataRepositoryError> { [weak self] promise in
            guard let self = self else { promise(.failure(.customError)) ; return }
            self.callGetPlaces(address: address)
                .share()
                .subscribe(on: DispatchQueue.global(qos: .background))
                .receive(on: DispatchQueue.main)
                .sink { [weak self] completion in
                    self?.handleGetPlaces(completion, promise: promise)
                } receiveValue: { result in
                    promise(.success(result))
                }
                .store(in: &self.cancellables)
        }
        .eraseToAnyPublisher()
    }

    func handleGetPlaces(
        _ completion: Subscribers.Completion<Error>,
        promise: (Result<DataModel, DataRepositoryError>) -> Void
    ) {
        if case .failure(let error) = completion {
            print("\(error)")
            promise(.failure(.customError))
        }
    }

    func callGetPlaces(address: String) ->  AnyPublisher<DataModel, Error> {
        let endpoint = Constant.baseURL + address
        let url = URL(string: endpoint)

        return networkService.fetch(type: DataModel.self,
                                    url: url)
    }
}
