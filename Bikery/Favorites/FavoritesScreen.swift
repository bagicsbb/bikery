//
//  FavoritesScreen.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 31..
//

import SwiftUI

struct FavoritesScreen: View {
    
    @ObservedObject var viewModel: FavoritesScreenViewModel
    
    var body: some View {
        NavigationView {
            list
                .onAppear(perform: {
                    viewModel.loadData()
                })
                .navigationTitle(Constant.favoritesScreenTitle)
                .navigationBarTitleDisplayMode(.automatic)
        }
    }
    
    var list: some View {
        List {
            ForEach(viewModel.savedItems, id: \.id) { data in
                PlaceCell(viewModel: PlaceCellViewModel(data: data, updateData: viewModel.updateData, isFavoriteItem: viewModel.isFavorite(data: data)))
            }
            .onDelete { index in
                viewModel.removeItems(index: index)
            }
        }
        .listStyle(.plain)
    }
}

struct FavoritesScreen_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesScreen(viewModel: FavoritesScreenViewModel())
    }
}
