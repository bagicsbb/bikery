//
//  FavoritesScreenViewModel.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 31..
//

import Foundation
import Combine

class FavoritesScreenViewModel: ObservableObject {
    @Published var isLoading = false
    @Published var savedItems: [PlaceData] = []
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        addSubscriber()
    }
    
    func addSubscriber() {

        DataService.shared.$savedEntities
            .map { entities -> [PlaceData] in
                entities.compactMap { (entity) -> PlaceData? in
                    return PlaceData(name: entity.name ?? "",
                                     summary: entity.summary ?? "",
                                     type: .address,
                                     lat: entity.lat,
                                     lon: entity.lon,
                                     id: entity.id ?? "",
                                     iconURL: entity.iconURL ?? "")
                }
            }
            .sink { [weak self] placesData in
                self?.savedItems = placesData
            }
            .store(in: &cancellables)
    }
}

extension FavoritesScreenViewModel {
    
    //MARK: - DataService Functions
    
    func updateData(data: PlaceData) {
        DataService.shared.updateData(placeData: data)
    }
    
    func isFavorite(data: PlaceData) -> Bool {
        DataService.shared.isFavoriteItem(placeData: data)
    }
    
    func loadData() {
        DataService.shared.fetchData()
    }
    
    func removeItems(index: IndexSet) {
        for index in index {
            updateData(data: savedItems[index])
        }
    }
}
