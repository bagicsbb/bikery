//
//  DataService.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 31..
//

import Foundation
import CoreData

class DataService: ObservableObject {
    @Published var savedEntities: [DataEntity] = []
    let container: NSPersistentContainer
    static let shared = DataService()
    
    init() {
        container = NSPersistentContainer(name: Constant.dataModelName)
        container.loadPersistentStores { description, error in
            if let error = error {
                print(error)
            } else {}
            self.fetchData()
        }
    }
    
    func updateData(placeData: PlaceData) {
        
        if let entity = savedEntities.first(where: { $0.id == placeData.id}) {
            deleteEntity(entity: entity)
            fetchData()
        } else {
            addData(data: placeData)
        }
    }
    
    func isFavoriteItem(placeData: PlaceData) -> Bool {
        
        if let _ = savedEntities.first(where: { $0.id == placeData.id}) {
            return true
        } else {
            return false
        }
    }
    
    func fetchData() {
        let request = NSFetchRequest<DataEntity>(entityName: Constant.entityName)
        
        do {
            savedEntities = try container.viewContext.fetch(request)
            print(savedEntities)
        } catch {
            //Error handling
            print(error)
        }
    }
    
    private func deleteEntity(entity: DataEntity) {
        container.viewContext.delete(entity)
        saveData()
        
    }
    
    private func addData(data: PlaceData) {
        let entity = DataEntity(context: container.viewContext)
        entity.name = data.name
        entity.iconURL = data.iconURL
        entity.id = data.id
        entity.lon = data.lon
        entity.lat = data.lat
        entity.summary = data.summary

        saveData()
        fetchData()
    }
    
    private func saveData() {
        do {
            try container.viewContext.save()
            fetchData()
        } catch let error {
            //Error handling
            print(error)
        }
    }
}
