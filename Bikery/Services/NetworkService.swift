//
//  NetworkManager.swift
//  Bikery
//
//  Created by Macbook Pro CTO on 2022. 01. 30..
//

import Foundation
import Combine

class NetworkService {
    private var cancellables = Set<AnyCancellable>()
    private var session: URLSession = URLSession.shared
    
    
    func fetch<T: Decodable>(type: T.Type, url: URL?) -> AnyPublisher<T, Error> {
        guard let url = url else {
            fatalError()
        }

        let urlRequest = URLRequest(url: url)

        return session.dataTaskPublisher(for: urlRequest)
            .tryMap { element -> Data in
                if let httpResponse = element.response as? HTTPURLResponse {
                    print(httpResponse)
                    let responseJSON = try? JSONSerialization.jsonObject(with: element.data, options: [])
                    if let responseJSON = responseJSON as? [String: Any] {
                        print(responseJSON)
                    }
                }
                return element.data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
